#! /bin/bash


# picosvox_config.sh --
#
#   This file permits to configure language of PicoSVox for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


clear

nom_distribution="Emmabuntus Debian Edition 5"

fichier_init_config_picosvox=~/.picosvoxooo

if ! [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then
    /opt/gSpeech/autostart_gspeech.sh
fi

if [[ -f $fichier_init_config_picosvox ]]
then

language_picosvox="fr-FR"

    if [[ $LANG == fr* ]] ; then

        language_picosvox="fr-FR"

    elif [[ $LANG == it* ]] ; then

        language_picosvox="it-IT"

    elif [[ $LANG == es* ]] ; then

        language_picosvox="es-ES"

    elif [[ $LANG == de* ]] ; then

        language_picosvox="de-DE"

    elif [[ $LANG == en_GB* ]] ; then

        language_picosvox="en-GB"

    else

        language_picosvox="en-US"
    fi

    if [[ $(grep -e "$language_picosvox" ${fichier_init_config_picosvox}) ]]
    then

        echo "Langue $language_picosvox ready"

    else

        # change language
        echo "Change Langue to $language_picosvox "
        sed s/\'__language__\':\ \'..-..\'/\'__language__\':\ \'$language_picosvox\'/ ${fichier_init_config_picosvox} > ${fichier_init_config_picosvox}.tmp
        cp ${fichier_init_config_picosvox}.tmp ${fichier_init_config_picosvox}
        rm ${fichier_init_config_picosvox}.tmp

    fi



fi





