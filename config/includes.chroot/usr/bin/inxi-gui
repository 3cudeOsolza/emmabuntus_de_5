#! /bin/bash
#--------------------------------------------------------------------------------------------------------
# Info tool by Misko_2083
#
# modified by kmathern to use yad
#--------------------------------------------------------------------------------------------------------

main_window_text="Select the info tool you wish to use, then click the OK button."
main_window_select="Select"
main_window_name="Name"
main_window_command="Command"
main_window_description="Description"
button_ok="OK"
button_exit="Exit"
button_save="Save"
button_cancel="Cancel"


text_disk_df="df - View file system disk space usage\nYou can select the mount to open in the file manager"
text_nothing="Nothing was selected."

disk_df_name="Fs disk space info"
disk_df_description="View filesystem disk space usage"

FDISK_name="List Partition Tables"
FDISK_description="List out the partition information (password required)"

BLOCKDEV_name="Display Block Devices"
BLOCKDEV_description="List out information of all block devices"

PARTED_name="List Partition Layout"
PARTED_description="List partition layout on all block devices"

lspci_info_name="PCI info"
lspci_info_description="View PCI devices info"

lspci_graph_name="Graphics"
lspci_graph_description="View graphics devices info"

lsusb_info_name="USB info"
lsusb_info_description="View usb devices info"

CPU_name="32/64 bit CPU"
CPU_description="Find out is this 32 or 64 bit CPU"

CPUZ_name="Processor info"
CPUZ_description="Display detailed info on CPU"

OS_name="32/64 bit OS"
OS_description="Find out is this 32 or 64 bit OS"

inxi_full_name="Full info"
inxi_full_description="View system info"

inxi_df_name="Partition info"
inxi_df_description="View partition info"

REPOS_name="View Repositories"
REPOS_description="View repositories on this system"

GRAPHICS_name="View Graphics"
GRAPHICS_description="View graphics on this system"

AUDIO_name="View Audio"
AUDIO_description="View audio on this system"

NETWORK_name="View Network"
NETWORK_description="View network on this system"

NETWORKC_name="Network Configuration"
NETWORKC_description="View network  configuration on this system"

OPENGL_name="View OpenGL configuration"
OPENGL_description="View OpenGL configuration on this system"

LSB_name="View lsb release"
LSB_description="View distribution-specific information"


########################################################################

if [[ $LANG == fr* ]] ; then

main_window_text="Sélectionnez l'outil d'information que vous souhaitez utiliser, puis cliquez sur le bouton OK."
main_window_select="Selection"
main_window_name="Nom"
main_window_command="Commande"
main_window_description="Description"
button_ok="OK"
button_exit="Quitter"
button_save="Sauver"
button_cancel="Annuler"


text_disk_df="df - Afficher l'utilisation de l'espace disque du système de fichiers\nVous pouvez sélectionner le montage à ouvrir dans le gestionnaire de fichiers"
text_nothing="Vous n'avez pas sélectionné une commande."

disk_df_name="Informations sur l'espace disque FS"
disk_df_description="Affiche l'utilisation de l'espace disque du système de fichiers"

FDISK_name="Liste des tables de partition"
FDISK_description="Liste les informations de partition (mot de passe requis)"

BLOCKDEV_name="Afficher les périphériques de bloc"
BLOCKDEV_description="Liste les informations de tous les périphériques bloqués"

PARTED_name="Liste de la disposition des partitions"
PARTED_description="Répertorier la disposition des partitions sur tous les périphériques de bloc"

lspci_info_name="Informations PCI"
lspci_info_description="Afficher les informations sur les périphériques PCI"

lspci_graph_name="Graphiques"
lspci_graph_description="Afficher les informations sur les périphériques graphiques"

lsusb_info_name="Informations USB"
lsusb_info_description="Afficher les informations sur les périphériques USB"

CPU_name="CPU 32/64 bits"
CPU_description="Découvrez qu'il s'agit de ce processeur 32 ou 64 bits"

CPUZ_name="Informations sur le processeur"
CPUZ_description="Afficher des informations détaillées sur le CPU"

OS_name="OS 32/64 bits"
OS_description="Découvrez qu'il s'agit de ce système d'exploitation 32 ou 64 bits"

inxi_full_name="Informations complètes"
inxi_full_description="Afficher les informations système"

inxi_df_name="Informations de partition"
inxi_df_description="Afficher les informations sur la partition"

REPOS_name="Afficher les référentiels"
REPOS_description="Afficher les référentiels sur ce système"

GRAPHICS_name="Afficher les graphiques"
GRAPHICS_description="Afficher les graphiques sur ce système"

AUDIO_name="Afficher l'audio"
AUDIO_description="Voir l'audio sur ce système"

NETWORK_name="Afficher le réseau"
NETWORK_description="Afficher le réseau sur ce système"

NETWORKC_name="Configuration réseau"
NETWORKC_description="Afficher la configuration réseau sur ce système"

OPENGL_name="Afficher la configuration OpenGL"
OPENGL_description="Afficher la configuration OpenGL sur ce système"

LSB_name="Afficher la version lsb"
LSB_description="Afficher les informations spécifiques à la distribution"

fi

###########################################################################################

disk_df=(FALSE "${disk_df_description}" "df -Th | grep '^/dev/'")

FDISK=(FALSE "${FDISK_description}" "sudo fdisk -l")

BLOCKDEV=(FALSE "${BLOCKDEV_description}" "lsblk")

PARTED=(FALSE "${PARTED_description}" "sudo parted -l")

lspci_info=(FALSE "${lspci_info_description}" "lspci -vnn" )

lspci_graph=(FALSE "${lspci_graph_description}" "lspci -vnn | grep -i 'VGA\|3D\|Display' -A 12" )

lsusb_info=(FALSE "${lsusb_info_description}" "lsusb" )

CPU=(FALSE "${CPU_description}" "egrep -c \ lm\  /proc/cpuinfo")

CPUZ=(FALSE "${CPUZ_description}" "lscpu")

OS=(FALSE "${OS_description}" "uname -a")

inxi_full=(FALSE "${inxi_full_description}" "inxi -Fxxxza --no-host")

inxi_df=(FALSE "${inxi_df_description}" "inxi -plu")

REPOS=(FALSE "${REPOS_description}" "inxi -r")

GRAPHICS=(FALSE "${GRAPHICS_description}" "inxi -Gxx")

AUDIO=(FALSE "${AUDIO_description}" "inxi -A")

NETWORK=(FALSE "${NETWORK_description}" "inxi -nz")

NETWORKC=(FALSE "${NETWORKC_description}" "ip addr show")

OPENGL=(FALSE "${OPENGL_description}" "glxinfo | grep OpenGL")

LSB=(FALSE "${LSB_description}" "lsb_release -idrc")


function_disk_df(){
   SOME_TEXT="${text_disk_df}"
   SOME_TITLE="df"

   df -h -T| tail -n+2 | while read fs type size used rest target; do
      if [[ $rest ]] ; then
         echo "$fs" "$type" "$size"B "$used"B "$rest"B "${target[@]}" |  grep '^/dev/' | # remove "grep /dev/sd |" to show all
         awk '{print $1,"\n",$2,"\n",$3,"\n",$4,"\n",$5,"\n",$6}BEGIN{ s = 7; e = 35; }{for (i=s; i<=e; i++) printf("%s%s", $(i), i<e ? OFS : "\n"); }' #Workaround for disk labels that contain whitespaces(number of characters that can be divided by whitespace =e-s)
      fi
   #done | sed -e 's/[ \t]*$//' $1|yad --center --list --width=685 --height=350 --title="${SOME_TITLE}" --text="${SOME_TEXT}" --column="Device" --column="Type" --column="Size" --column="Used" --column="Free" --column="%Used" --column="Mount" --print-column="7"| cut -d '|' -f1| tee /tmp/tempdf

   #With radiolist is also an option
   done | sed -e 's/[ \t]*$//' $1| sed 's!^/dev/sd*!FALSE\n/dev/sd!g' | yad --center --list --radiolist --width=685 --height=350 --title="${SOME_TITLE}" --text="${SOME_TEXT}" --column="Select" --column="FS" --column="Type" --column="Size" --column="Used" --column="Free" --column="%Used" --column="Mount" --print-column="8"| cut -d '|' -f1| tee /tmp/tempdf
   if [ -z "$(cat /tmp/tempdf)" ]; then
   rm -f /tmp/tempdf
   #exit 0
   return
   fi
   xdg-open "$(cat /tmp/tempdf)"
   rm -f /tmp/tempdf
}

function_FDISK(){
   gksu 'sudo fdisk -l' | tee /tmp/fdiskinfo| yad --center --title="Partition Tables (sudo fdisk -l)" --text-info --width=800 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/fdiskinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --confirm-overwrite --filename="Partition_Tables.txt" --title="Save List of the Partition Tables")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/fdiskinfo
}

function_BLOCKDEV(){
   lsblk  |tee /tmp/lsblkinfo | yad --center --title="Block Devices (lsblk)" --text-info --width=800 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/lsblkinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Block_Devices.txt" --title="Save Block Devices info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/lsblkinfo
}

function_PARTED(){
   gksu --message 'To run this tool your password is required. Enter your password (user password), or press Cancel.' 'sudo parted -l'|tee /tmp/partedinfo | yad --center --title="Partition Layout (sudo parted -l)" --text-info --width=800 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/partedinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Partition_layout.txt" --title="Save Partition Layout info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/partedinfo
}

function_lspci_info(){
   lspci -vnn | tee /tmp/lspciinfo | yad --center --title="PCI info (lspci -vnn)" --text-info --width=800 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/lspciinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="PCI_Devices.txt" --title="Save PCI Devices info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/lspciinfo
}

function_lspci_graph(){
   lspci -vnn | grep VGA -A 12 | tee /tmp/lspcigraph| yad --center --title="Graphics (lspci -vnn | grep -i 'VGA\|3D\|Display' -A 12)" --text-info --width=800 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[3]}" -ne "1" ]; then
      zNewData=$(cat /tmp/lspcigraph)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Graphics_info.txt" --title="Save Graphics info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/lspcigraph
}

function_lsusb_info(){
   lsusb | tee /tmp/lsusbinfo | yad --center --title="USB info (lsusb)" --text-info --width=800 --height=400 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/lsusbinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="USB_info.txt" --title="Save USB info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/lsusbinfo
}

function_CPU(){
   if [ "$(egrep -c ' lm ' /proc/cpuinfo)" -lt "1" ]; then
      bus_zen="32"
   else
      bus_zen="64"
   fi
      yad --center --title="32/64 bit CPU" --info --text="This is $bus_zen bit CPU"
}

function_CPUZ(){
   lscpu | tee /tmp/processorinfo | yad --center --title="Processor info (lscpu)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/processorinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="CPU_info.txt" --title="Save Processor info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/processorinfo
}

function_OS(){
   if [ "$(uname -a | egrep -c 'i386|i486|i586|i686')" -eq "1" ]; then
      os_zen="32"
   else
      os_zen="64"
   fi
      yad --center --title="32/64 bit OS" --info --text="This is $os_zen bit OS"
}

function_inxi_full(){
   inxi -Fxxxza --no-host -c 0 | tee /tmp/inxifull | yad --center --title="Full info (inxi -Fxxxza --no-host)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/inxifull)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Full_info.txt" --title="Save Full Computer info")")
      echo -n "$zNewData" > "$zSavePath"

   fi
      rm /tmp/inxifull
}

function_inxi_df(){
   inxi -plu -c 0 | tee /tmp/partitionsinfo| yad --center --title="Partition info (inxi -plu)" --text-info --width=650 --height=400 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/partitionsinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Partition_info.txt" --title="Save Partition info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/partitionsinfo
}

function_REPOS(){
   inxi -r -c 0 | tee /tmp/repositorieslist | yad --center --title="Repositories (inxi -r)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/repositorieslist)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Repositories.txt" --title="Save List Of The Repositories")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/repositorieslist
}

function_GRAPHICS(){
   inxi -Gxx -c 0 | tee /tmp/graphicsinfo | yad --center --title="Graphics info (inxi -Gxx)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/graphicsinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Graphics_inxi.txt" --title="Save Graphic information (inxi)")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/graphicsinfo
}

function_AUDIO(){
   inxi -A -c 0 | tee /tmp/audioinfo | yad --center --title="Audio info (inxi -A)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/audioinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Audio_info.txt" --title="Save Audio information (inxi)")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/audioinfo
}

function_NETWORK(){
   inxi -nz -c 0 | tee /tmp/networkinfo | yad --center --title="Network info (inxi -nz)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/networkinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Network_inxi.txt" --title="Save Network information (inxi)")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/networkinfo
}

function_NETWORKC(){
   /sbin/ip addr show | tee /tmp/networkconfinfo | yad --center --title="Network info (ip addr show)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/networkconfinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="Network_conf.txt" --title="Save Network Configuration Info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/networkconfinfo
}

function_OPENGL(){
   glxinfo | grep OpenGL | tee /tmp/openglinfo | yad --center --title="View OpenGL configuration (glxinfo | grep OpenGL)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[3]}" -ne "1" ]; then
      zNewData=$(cat /tmp/openglinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="OpenGL_info.txt" --title="Save OpenGL Info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/openglinfo
}

function_LSB(){
   lsb_release -idrc| tee /tmp/lsbinfo | yad --center --title="View lsb release (lsb_release -idrc)" --text-info --width=850 --height=600 --button ${button_save}:0 --button ${button_cancel}:1
   if [ "${PIPESTATUS[2]}" -ne "1" ]; then
      zNewData=$(cat /tmp/lsbinfo)
      zSavePath=$(echo -n "$(yad --center --file-selection --save --button ${button_save}:0 --button ${button_cancel}:1 --confirm-overwrite --filename="ls_release.txt" --title="Save ls_ release Info")")
      echo -n "$zNewData" > "$zSavePath"
   fi
      rm /tmp/lsbinfo
}


function_nothing(){
   yad --center --info --width=200 --height=50 --title='Info' --text="${text_nothing}" --button OK:0
}



redraw_main_window="1"
while [ $redraw_main_window == "1" ]; do
  redraw_main_window="0"
  selection=$(yad \
  --center \
  --width=900 \
  --height=560 \
  --text="${main_window_text}" \
  --title="PC Information" \
  --window-icon="computer" \
  --button ${button_ok}:0 \
  --button ${button_exit}:1 \
  --list \
    --radiolist \
    --column="${main_window_select}" \
    --column="${main_window_description}" \
    --column="${main_window_command}" \
    "${disk_df[@]}" \
    "${FDISK[@]}" \
    "${BLOCKDEV[@]}" \
    "${PARTED[@]}" \
    "${lspci_info[@]}" \
    "${lspci_graph[@]}" \
    "${lsusb_info[@]}" \
    "${CPU[@]}" \
    "${CPUZ[@]}" \
    "${OS[@]}" \
    "${inxi_full[@]}" \
    "${inxi_df[@]}" \
    "${REPOS[@]}" \
    "${GRAPHICS[@]}" \
    "${AUDIO[@]}" \
    "${NETWORK[@]}" \
    "${NETWORKC[@]}" \
    "${OPENGL[@]}" \
    "${LSB[@]}" )

  if [ "$?" -eq 0 ];
    then
      if [ "${selection[@]}" == "" ]
        then
          function_nothing
          redraw_main_window="1"
        else
          :
      fi
    else
      exit 0
  fi

  while [ "${selection[@]}" != "" ] ; do

    case $(cut -f2 -d'|' <<<"${selection[@]}") in

          ${disk_df[1]}) function_disk_df
                         selection=''
                         redraw_main_window="1"
                         ;;

            ${FDISK[1]}) function_FDISK
                         selection=''
                         redraw_main_window="1"
                         ;;

         ${BLOCKDEV[1]}) function_BLOCKDEV
                         selection=''
                         redraw_main_window="1"
                         ;;

           ${PARTED[1]}) function_PARTED
                         selection=''
                         redraw_main_window="1"
                         ;;

       ${lspci_info[1]}) function_lspci_info
                         selection=''
                         redraw_main_window="1"
                         ;;

      ${lspci_graph[1]}) function_lspci_graph
                         selection=''
                         redraw_main_window="1"
                         ;;

       ${lsusb_info[1]}) function_lsusb_info
                         selection=''
                         redraw_main_window="1"
                         ;;

              ${CPU[1]}) function_CPU
                         selection=''
                         redraw_main_window="1"
                         ;;

             ${CPUZ[1]}) function_CPUZ
                         selection=''
                         redraw_main_window="1"
                         ;;

               ${OS[1]}) function_OS
                         selection=''
                         redraw_main_window="1"
                         ;;

        ${inxi_full[1]}) function_inxi_full
                         selection=''
                         redraw_main_window="1"
                         ;;

          ${inxi_df[1]}) function_inxi_df
                         selection=''
                         redraw_main_window="1"
                         ;;

            ${REPOS[1]}) function_REPOS
                         selection=''
                         redraw_main_window="1"
                         ;;

         ${GRAPHICS[1]}) function_GRAPHICS
                         selection=''
                         redraw_main_window="1"
                         ;;

            ${AUDIO[1]}) function_AUDIO
                         selection=''
                         redraw_main_window="1"
                         ;;

          ${NETWORK[1]}) function_NETWORK
                         selection=''
                         redraw_main_window="1"
                         ;;

         ${NETWORKC[1]}) function_NETWORKC
                         selection=''
                         redraw_main_window="1"
                         ;;

           ${OPENGL[1]}) function_OPENGL
                         selection=''
                         redraw_main_window="1"
                         ;;

              ${LSB[1]}) function_LSB
                         selection=''
                         redraw_main_window="1"
                         ;;

  esac
  sleep 1
  done
done
